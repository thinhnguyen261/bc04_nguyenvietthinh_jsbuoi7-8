var array = [];

function themSoVaoMang() {
  var so = document.getElementById("txt-so").value * 1;
  array.push(so);
  document.getElementById("mang").innerHTML = `${array}`;
}

function tinhTongSoDuong() {
  var tong = 0;
  array.forEach((el) => {
    if (el > 0) {
      tong = tong + el;
    }
  });
  document.getElementById("tongsoduong").innerHTML = `${tong}`;
}

function demSoDuong() {
  var dem = 0;
  array.forEach((el) => {
    if (el > 0) {
      dem = dem + 1;
    }
  });
  document.getElementById("demsoduong").innerHTML = `${dem}`;
}

function timSoNhoNhat() {
  var soNhoNhat = array[0];
  array.forEach((el) => {
    if (el < soNhoNhat) {
      soNhoNhat = el;
    }
  });
  document.getElementById("sonhonhat").innerHTML = `${soNhoNhat}`;
}

function timSoDuongNhoNhat() {
  var soDuongNhoNhat = null;
  array.forEach((el) => {
    if (el > 0) {
      if (soDuongNhoNhat == null) {
        soDuongNhoNhat = el;
      }
      if (el < soDuongNhoNhat) {
        soDuongNhoNhat = el;
      }
    }
  });
  document.getElementById("soduongnhonhat").innerHTML = `${soDuongNhoNhat}`;
}

function timSoChanCuoiCung() {
  var soChanCuoiCung = null;
  array.forEach((el) => {
    if (el % 2 == 0) {
      soChanCuoiCung = el;
    }
  });
  document.getElementById("sochancuoicung").innerHTML = `${soChanCuoiCung}`;
}

function doiCho() {
  var viTriA = document.getElementById("txt-vitri1").value * 1;
  var viTriB = document.getElementById("txt-vitri2").value * 1;

  var giaTriA = array[viTriA];
  var giaTriB = array[viTriB];
  var giaTriTrungGian = giaTriA;

  array[viTriA] = giaTriB;
  array[viTriB] = giaTriTrungGian;

  document.getElementById("doicho").innerHTML = `${array}`;
}
